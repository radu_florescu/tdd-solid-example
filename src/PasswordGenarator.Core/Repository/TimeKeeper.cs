﻿using System;

namespace PasswordGenarator.Core.Repository
{
    public class TimeKeeper:ITimeKeeper
    {
        public DateTime Now {
            get
            {
                return DateTime.UtcNow;
            }
        }
    }
}
