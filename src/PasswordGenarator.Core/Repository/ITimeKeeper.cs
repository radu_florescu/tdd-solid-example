﻿using System;

namespace PasswordGenarator.Core.Repository
{
    public interface ITimeKeeper
    {
        DateTime Now { get; }
    }
}
