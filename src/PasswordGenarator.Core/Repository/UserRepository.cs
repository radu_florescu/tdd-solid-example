using System;
using System.Linq;
using System.Transactions;
using PasswordGenarator.Core.Context;

namespace PasswordGenarator.Core.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly UserDbContext _context;
        private readonly ITimeKeeper _timeKeeper;


        internal UserRepository(UserDbContext context, ITimeKeeper timeKeeper)
        {
            _context = context;
            _timeKeeper = timeKeeper;
        }

        public UserRepository(UserDbContext context)
            : this(context, new TimeKeeper())
        {
            _context = context;
        }

        public string CreatePassword(string username)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException("username");

            string generatedPassword;

            using (var transaction = new TransactionScope(TransactionScopeOption.Required))
            {
                var user = _context.Users.FirstOrDefault(u => u.Username == username);

                if (user == null)
                {
                    user = new User
                    {
                        Username = username,
                        Password = new Password()
                        {
                            ValidUntil = _timeKeeper.Now.AddSeconds(30)
                        }
                    };

                    _context.Users.Add(user);
                }

                generatedPassword = System.Web.Security.Membership.GeneratePassword(6, 1);
                user.Password.PasswordHash = generatedPassword;

                _context.SaveChanges();
                transaction.Complete();
            }

            return generatedPassword;
        }

        public bool CheckPassword(string username, string password)
        {
            var user = _context.Users.FirstOrDefault(u => u.Username == username && u.Password.PasswordHash == password);

            return user != null && _timeKeeper.Now <= user.Password.ValidUntil;
        }
    }
}