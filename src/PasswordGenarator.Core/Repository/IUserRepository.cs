﻿namespace PasswordGenarator.Core.Repository
{
    public interface IUserRepository
    {
        string CreatePassword(string username);
        bool CheckPassword(string username, string password);
    }
}
