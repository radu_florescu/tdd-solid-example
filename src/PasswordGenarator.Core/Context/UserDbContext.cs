﻿using System.Data.Entity;

namespace PasswordGenarator.Core.Context
{
    public class UserDbContext : DbContext
    {
        public UserDbContext(string connectionStringName = "DefaultConnection") : base(connectionStringName)
        {

        }

        public DbSet<User> Users { get; set; }

        public DbSet<Password> Passwords { get; set; }
    }
}