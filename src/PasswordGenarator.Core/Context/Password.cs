using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PasswordGenarator.Core.Context
{
    public class Password
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid PasswordId { get; set; }

        public String PasswordHash { get; set; }
        public DateTime ValidUntil { get; set; }
    }
}