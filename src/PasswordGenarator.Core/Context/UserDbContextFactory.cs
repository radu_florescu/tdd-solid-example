﻿using System.Data.Entity.Infrastructure;

namespace PasswordGenarator.Core.Context
{
    public class UserDbContextFactory : IDbContextFactory<UserDbContext>
    {
        public UserDbContext Create()
        {
            return new UserDbContext();
        }
    }
}
