using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PasswordGenarator.Core.Context
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid UserId { get; set; }

        public string Username { get; set; }

        public virtual Guid PasswordId { get; set; }

        public virtual Password Password { get; set; }
    }
}