﻿using System.Web.Mvc;
using PasswordGenarator.Core.Repository;
using PasswordGenerator.Web.Models;

namespace PasswordGenerator.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserRepository _userRepository;

        public HomeController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ValidatePassword()
        {
            return View(new ValidatePasswordModel());
        }

        [HttpPost]
        public ActionResult ValidatePassword(ValidatePasswordModel passwordModel)
        {
            if (!ModelState.IsValid)
                return View(passwordModel);

            if (_userRepository.CheckPassword(passwordModel.Username, passwordModel.Password))
                return View("Success");

            ModelState.AddModelError("", "Invalid user or password");
            return View(passwordModel);
        }


        public ActionResult GeneratePassword()
        {
            return View(new GeneratePasswordModel());
        }

        [HttpPost]
        public ActionResult GeneratePassword(GeneratePasswordModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            string password = _userRepository.CreatePassword(model.Username);

            if (!string.IsNullOrEmpty(password))
                return RedirectToAction("ValidatePassword", new ValidatePasswordModel() { Username = model.Username, Password = password });

            ModelState.AddModelError("", "Password was empty");
            return View(model);
        }
    }

}