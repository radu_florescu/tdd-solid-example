﻿using System.ComponentModel.DataAnnotations;

namespace PasswordGenerator.Web.Models
{
    public class GeneratePasswordModel
    {
        [Required]
        public string Username { get; set; }
    }
}