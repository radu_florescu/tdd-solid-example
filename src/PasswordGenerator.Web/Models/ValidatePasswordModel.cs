﻿namespace PasswordGenerator.Web.Models
{
    public class ValidatePasswordModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}