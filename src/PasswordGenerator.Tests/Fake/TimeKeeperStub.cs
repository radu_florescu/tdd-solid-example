﻿using System;
using PasswordGenarator.Core.Repository;

namespace PasswordGenerator.Tests.Fake
{
    internal class TimeKeeperStub : ITimeKeeper
    {
        private DateTime _now;

        public TimeKeeperStub()
        {
            _now = DateTime.UtcNow;
        }

        public void ForwardTime(int numberOfSeconds)
        {
            _now = _now.AddSeconds(numberOfSeconds);
        }

        public DateTime Now {
            get
            {
                return _now;
            }
        }
    }
}
