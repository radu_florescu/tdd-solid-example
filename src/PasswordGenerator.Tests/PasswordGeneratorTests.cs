﻿using System;
using System.Threading;
using NUnit.Framework;
using PasswordGenarator.Core.Context;
using PasswordGenarator.Core.Repository;
using PasswordGenerator.Tests.Fake;

namespace PasswordGenerator.Tests
{

    [TestFixture]
    public class PasswordGeneratorTests
    {
        [Test]
        public void should_generate_the_password_for_a_given_user_id()
        {
            const string username = "someUser";
            UserDbContext userDbContext = new UserDbContext();
            IUserRepository userRepository = new UserRepository(userDbContext);

            string password = userRepository.CreatePassword(username);

            Assume.That(password, Is.Not.Null, "Password must not be null");
            Assert.That(password, Is.Not.Empty, "Password must not be empty");
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        public void should_throw_exception_when_for_a_given_empty_user_id(string username)
        {
            UserDbContext userDbContext = new UserDbContext();
            IUserRepository userRepository = new UserRepository(userDbContext);

            Assert.Throws<ArgumentNullException>(() => userRepository.CreatePassword(username), "Username must not be empty");
        }

        [Test]
        public void should_generate_a_new_user_password_given_the_same_user_name()
        {
            const string username = "someUser";
            UserDbContext userDbContext = new UserDbContext();
            IUserRepository userRepository = new UserRepository(userDbContext);

            string password = userRepository.CreatePassword(username);

            Assume.That(password, Is.Not.Null, "Password must not be null");
            Assume.That(password, Is.Not.Empty, "Password must not be empty");

            string newPassword = userRepository.CreatePassword(username);

            Assert.That(newPassword, Is.Not.EqualTo(password));
        }

        [Test]
        public void should_verify_user_password_is_valid()
        {
            string username = "someUser" + DateTime.UtcNow.Ticks;
            UserDbContext userDbContext = new UserDbContext();
            IUserRepository userRepository = new UserRepository(userDbContext);

            string password = userRepository.CreatePassword(username);

            Assume.That(password, Is.Not.Null, "Password must not be null");
            Assume.That(password, Is.Not.Empty, "Password must not be empty");

            bool isValidPassword = userRepository.CheckPassword(username, password);

            Assert.That(isValidPassword, Is.True);
        }

        [Test]
        [TestCase(29, true)]
        [TestCase(30, true)]
        [TestCase(31, false)]
        public void should_verify_user_password_is_valid_for_an_ammount_of_seconds(int seconds, bool expectedValidationResult)
        {
            string username = "someUser" + DateTime.UtcNow.Ticks;
            UserDbContext userDbContext = new UserDbContext();
            TimeKeeperStub timeKeeper = new TimeKeeperStub();

            IUserRepository userRepository = new UserRepository(userDbContext, timeKeeper);

            string password = userRepository.CreatePassword(username);

            Assume.That(password, Is.Not.Null, "Password must not be null");
            Assume.That(password, Is.Not.Empty, "Password must not be empty");

            bool isValidPassword = userRepository.CheckPassword(username, password);

            Assume.That(isValidPassword, Is.True);
            timeKeeper.ForwardTime(seconds);
            isValidPassword = userRepository.CheckPassword(username, password);

            Assert.That(isValidPassword, Is.EqualTo(expectedValidationResult));
        }
    }
}
