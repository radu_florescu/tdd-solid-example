Write a program or API that can generate a one-time password, and verify if the password is valid for just the one user. 
The input of the program should be: 
User ID to generate the password, and the User ID and Password to verify the validity of the password.
Every generated password should be valid for only 30 seconds.

Setup instructions:

0) restore nuget packages
1) create a database connection that points to an empty database
2) go to PasswordGenerator\app.config and replace the existing connection with yours
2*) you need to update it in Tests\PasswordGenerator.Tests\app.config
3) open a package manager console
4) type: update-database
5) run unit tests
6) open web project in a browser
7) test the same functionality on web
